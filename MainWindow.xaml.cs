﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
//Naming convention: https://wpf.2000things.com/2017/07/25/1214-naming-scheme-for-xaml-elements/

namespace RegistrySettingup
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum HKEY { HKEY_CLASSES_ROOT = 0, HKEY_CURRENT_USER = 1, HKEY_LOCAL_MACHINE = 2, HKEY_USERS = 3 , HKEY_CURRENT_CONFIG = 4 };

        public MainWindow()
        {
            InitializeComponent();
            Get();
        }

        public static string Int32ToHex(Int32 value)
        {
            return String.Format("dword:{0:X8}", value);
        }

        public static Int32 HexToInt32(string value)
        {
            // strip the leading dword:
            if (value.StartsWith("dword:", System.StringComparison.OrdinalIgnoreCase)) value = value.Substring(6);

            return Int32.Parse(value, System.Globalization.NumberStyles.HexNumber);
        }

        private (RegistryKey RegKey, RegistryKey SubRegKey) OpenKey()
        {
            //Reference the way to return multiple variables: https://stackoverflow.com/questions/748062/return-multiple-values-to-a-method-caller
            RegistryKey RegKey, SubRegKey;

            switch (this.cboKey.SelectedIndex)
            {
                case (int)HKEY.HKEY_CLASSES_ROOT:
                    RegKey = Registry.ClassesRoot;
                    break;
                case (int)HKEY.HKEY_CURRENT_USER:
                    RegKey = Registry.CurrentUser;
                    break;
                case (int)HKEY.HKEY_LOCAL_MACHINE:
                    RegKey = Registry.LocalMachine;
                    break;
                case (int)HKEY.HKEY_USERS:
                    RegKey = Registry.Users;
                    break;
                case (int)HKEY.HKEY_CURRENT_CONFIG:
                    RegKey = Registry.CurrentConfig;
                    break;
                default:
                    RegKey = null;
                    break;
            }
            SubRegKey = RegKey.OpenSubKey(this.txtSubKey.Text, true);
            if (SubRegKey == null)
            {
                RegKey.CreateSubKey(this.txtSubKey.Text, true);
                SubRegKey = RegKey.OpenSubKey(this.txtSubKey.Text, true);
            }

            return (RegKey, SubRegKey);
        }

        private void Set()
        {
            (RegistryKey RegKey, RegistryKey SubRegKey) = OpenKey();

            //Set String value.
            SetNameValueFromTextBoxes(SubRegKey, this.tbSoftwareVersionDate, this.txtSoftwareVersionDate, RegistryValueKind.String);
            SetNameValueFromTextBoxes(SubRegKey, this.tbManufacturerSerialNumber, this.txtManufacturerSerialNumber, RegistryValueKind.String);

            //Set DWord value: convert text of hex into int value, then set.
            SetNameValueFromTextBoxes(SubRegKey, this.tbDeviceProviderId, this.txtDeviceProviderId, RegistryValueKind.DWord);
            SetNameValueFromTextBoxes(SubRegKey, this.tbCompanyId, this.txtCompanyId, RegistryValueKind.DWord);
            SetNameValueFromTextBoxes(SubRegKey, this.tbStationId, this.txtStationId, RegistryValueKind.DWord);
            SetNameValueFromTextBoxes(SubRegKey, this.tbLineId, this.txtLineId, RegistryValueKind.DWord);
            SetNameValueFromTextBoxes(SubRegKey, this.tbDeviceId, this.txtDeviceId, RegistryValueKind.DWord);
            SetNameValueFromTextBoxes(SubRegKey, this.tbEquipmentType, this.txtEquipmentType, RegistryValueKind.DWord);
            SetNameValueFromTextBoxes(SubRegKey, this.tbEquipmentSubType, this.txtEquipmentSubType, RegistryValueKind.DWord);
            SetNameValueFromTextBoxes(SubRegKey, this.tbNetworkId, this.txtNetworkId, RegistryValueKind.DWord);
            SetNameValueFromTextBoxes(SubRegKey, this.tbSetMachineName, this.txtSetMachineName, RegistryValueKind.DWord);
            SetNameValueFromTextBoxes(SubRegKey, this.tbBOMode, this.txtBOMode, RegistryValueKind.DWord);
            SetNameValueFromTextBoxes(SubRegKey, this.tbRebootAtEnd, this.txtRebootAtEnd, RegistryValueKind.DWord);

            MessageBox.Show("The registries are set successfully.");
        }

        private void SetNameValueFromTextBoxes(RegistryKey RegKey, TextBlock textblock, TextBox textbox, RegistryValueKind type_obj)
        {
            switch (type_obj)
            {
                case RegistryValueKind.String:
                    RegKey.SetValue(textblock.Text, textbox.Text);
                    break;
                case RegistryValueKind.DWord:
                    RegKey.SetValue(textblock.Text, HexToInt32(textbox.Text)); // convert hex String into Int32 value to set registry
                    break;
            }
            
        }

        private void ResetTextBox(TextBox textbox, RegistryValueKind type_obj)
        {
            //Reset the TextBox into its default value: 0 if DWord TextBox or "" if String TextBox
            switch (type_obj)
            {
                case RegistryValueKind.String:
                    textbox.Text = (string)"";
                    break;
                case RegistryValueKind.DWord:
                    textbox.Text = Int32ToHex((Int32)0);
                    break;
            }
        }

        private void GetNameValueIntoTextBoxes(RegistryKey RegKey, TextBlock textblock, TextBox textbox, RegistryValueKind type_obj)
        {
            object value_obj = RegKey.GetValue(textblock.Text);

            if (value_obj == null) // if the name does not exist, we have to create empty name. Hence the corresponding TextBox must be reset to default value.
            {
                ResetTextBox(textbox, type_obj);
                SetNameValueFromTextBoxes(RegKey, textblock, textbox, type_obj);
                value_obj = RegKey.GetValue(textblock.Text);
            }
            switch (type_obj) // directly display String in TextBox or reformat DWord value be for displaying in TextBox
            {
                case RegistryValueKind.String:
                    textbox.Text = (string)value_obj;
                    break;
                case RegistryValueKind.DWord:
                    textbox.Text = Int32ToHex((Int32)value_obj);
                    break;
            }
        }

        private void Get()
        {
            (RegistryKey RegKey, RegistryKey SubRegKey) = OpenKey();

            GetNameValueIntoTextBoxes(SubRegKey, this.tbSoftwareVersionDate, this.txtSoftwareVersionDate, RegistryValueKind.String);
            GetNameValueIntoTextBoxes(SubRegKey, this.tbManufacturerSerialNumber, this.txtManufacturerSerialNumber, RegistryValueKind.String);

            GetNameValueIntoTextBoxes(SubRegKey, this.tbDeviceProviderId, this.txtDeviceProviderId, RegistryValueKind.DWord);
            GetNameValueIntoTextBoxes(SubRegKey, this.tbCompanyId, this.txtCompanyId, RegistryValueKind.DWord);
            GetNameValueIntoTextBoxes(SubRegKey, this.tbStationId, this.txtStationId, RegistryValueKind.DWord);                      
            GetNameValueIntoTextBoxes(SubRegKey, this.tbLineId, this.txtLineId, RegistryValueKind.DWord);
            GetNameValueIntoTextBoxes(SubRegKey, this.tbDeviceId, this.txtDeviceId, RegistryValueKind.DWord);
            GetNameValueIntoTextBoxes(SubRegKey, this.tbEquipmentType, this.txtEquipmentType, RegistryValueKind.DWord);
            GetNameValueIntoTextBoxes(SubRegKey, this.tbEquipmentSubType, this.txtEquipmentSubType, RegistryValueKind.DWord);
            GetNameValueIntoTextBoxes(SubRegKey, this.tbNetworkId, this.txtNetworkId, RegistryValueKind.DWord);
            GetNameValueIntoTextBoxes(SubRegKey, this.tbSetMachineName, this.txtSetMachineName, RegistryValueKind.DWord);
            GetNameValueIntoTextBoxes(SubRegKey, this.tbBOMode, this.txtBOMode, RegistryValueKind.DWord);
            GetNameValueIntoTextBoxes(SubRegKey, this.tbRebootAtEnd, this.txtRebootAtEnd, RegistryValueKind.DWord);
        }

        private void BtnSet_Click(object sender, RoutedEventArgs e)
        {
            Set();
        }

        private void BtnGet_Click(object sender, RoutedEventArgs e)
        {
            Get();
        }
    }
}
